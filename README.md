# Extensión para buscar terminos en el portal DARA desde la barra del navegador

# Descripción
Extensión para el navegador Chrome que permite buscar términos en el portal [DARA](http://dara.aragon.es/opac/app/home/) (Documentos y Archivos de Aragón) directamente desde la barra del navegador en lugar de tener que ir al portal en el navegador, rellenar el campo de texto y pulsar el botón de buscar.

# Uso
Simplemente ponga la palabra clave **dara** en la barra del navegador y pulse el espacio. A continuación indique los términos por los que desea buscar en el portal
 